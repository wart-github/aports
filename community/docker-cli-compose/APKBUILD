# Contributor: Jake Buchholz Göktürk <tomalok@gmail.com>
# Maintainer: Jake Buchholz Göktürk <tomalok@gmail.com>
pkgname=docker-cli-compose
pkgver=2.15.1
pkgrel=0
pkgdesc="A Docker CLI plugin for extended build capabilities"
url="https://docs.docker.com/compose/cli-command"
arch="all"
license="Apache-2.0"
depends="docker-cli"
makedepends="go"
options="net"
source="$pkgname-$pkgver.tar.gz::https://github.com/docker/compose/archive/v$pkgver.tar.gz"

# secfixes:
#   2.15.1-r0:
#     - CVE-2022-27664
#     - CVE-2022-32149
#   2.12.1-r0:
#     - CVE-2022-39253

_plugin_installdir="/usr/libexec/docker/cli-plugins"

builddir="$srcdir"/compose-"$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	PKG=github.com/docker/compose/v2
	local ldflags="-X '$PKG/internal.Version=v$pkgver'"
	go build -ldflags="$ldflags" -o docker-compose ./cmd
}

check() {
	# e2e tests are excluded because they depend on live dockerd/kubernetes/ecs
	local pkgs="$(go list ./... | grep -Ev '/e2e(/|$)')"
	go test -short $pkgs
	./docker-compose compose version
}

package() {
	install -Dm755 docker-compose -t "$pkgdir$_plugin_installdir"/
}

sha512sums="
b9252ae2185825269724b4c09773729bb39a8edf93ad47dfeead842896935647b9473fd20b45e648c81ba2ec93de87d2233b38c1aecc115266717c1285e29c11  docker-cli-compose-2.15.1.tar.gz
"
