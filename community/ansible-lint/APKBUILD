# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=ansible-lint
pkgver=6.11.0
pkgrel=0
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	ansible-core
	git
	py3-ansible-compat
	py3-filelock
	py3-jinja2
	py3-jsonschema
	py3-packaging
	py3-rich
	py3-ruamel.yaml
	py3-wcmatch
	py3-yaml
	python3
	yamllint
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
source="ansible-lint-$pkgver.tar.gz::https://github.com/ansible-community/ansible-lint/archive/refs/tags/v$pkgver/ansible-lint-v$pkgver.tar.gz"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
048745b84ee5c195cf2fb78a1a7ba72ce376d6fddc3298233985c8ef44744ec6f73c6ce60841ae708f59371a81973bb3c8ab35ae52c25c966e4289c487bcc220  ansible-lint-6.11.0.tar.gz
"
