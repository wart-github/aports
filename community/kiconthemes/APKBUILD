# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kiconthemes
pkgver=5.102.0
pkgrel=0
pkgdesc="Support for icon themes"
# armhf blocked by extra-cmake-module
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends_dev="
	karchive-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	qt5-qtsvg-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kiconthemes-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kiconloader_unittest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(kiconloader_unittest|kiconloader_resourcethemetest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ec119f27756d2e86adfbff06910a4c58b4035fc974d35cdee6a7292252bde0ab176b2737ac01db1e591dcf5b06f8ade05561a662903fd09922c841bb88d6f3e7  kiconthemes-5.102.0.tar.xz
"
