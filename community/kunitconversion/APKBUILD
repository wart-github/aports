# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kunitconversion
pkgver=5.102.0
pkgrel=0
pkgdesc="Support for unit conversion"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev ki18n-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kunitconversion-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
85d35a51ec900df0b5d9347073762727f166d4cef8dfa176df28e12a465fb056f6c7c56c7380d38a067fd70d95ba36c160633640872c9a2c2ed810eb565e3f47  kunitconversion-5.102.0.tar.xz
"
