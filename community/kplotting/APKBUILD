# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kplotting
pkgver=5.102.0
pkgrel=0
pkgdesc="Lightweight plotting framework"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev samurai"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kplotting-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
4a1e381f8ed03d31ea97b8f57dd8443fbb4afa6e9595d7b44c1f47528bc2c085639d0ee0947f580b35c2c495e961fffafc54bb6de14fd1857c0abab2dea71098  kplotting-5.102.0.tar.xz
"
