# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kxmlrpcclient
pkgver=5.102.0
pkgrel=0
pkgdesc="XML-RPC client library for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://projects.kde.org/projects/kde/pim/kxmlrpcclient"
license="BSD-2-Clause"
depends_dev="
	ki18n-dev
	kio-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kxmlrpcclient-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e4a748770e352cdf407a74ab3c36fec93e02a7911ed853e2d9a2621e34b06f7919c2d1b36137b6e60dd1b22ffdb2abb95878e583950bf0a74ddba475b68089d9  kxmlrpcclient-5.102.0.tar.xz
"
