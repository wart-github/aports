# Contributor: Keith Maxwell <keith.maxwell@gmail.com>
# Maintainer: Roberto Oliveira <robertoguimaraes8@gmail.com>
pkgname=py3-google-api-python-client
_pkgname=google-api-python-client
pkgver=2.74.0
pkgrel=0
pkgdesc="Google API Client Library for Python"
url="https://github.com/googleapis/google-api-python-client"
arch="noarch !ppc64le"  # limited by py3-grpcio
license="Apache-2.0"
depends="
	py3-google-api-core
	py3-google-auth
	py3-google-auth-httplib2
	py3-httplib2
	py3-oauth2client
	py3-uritemplate
	"
makedepends="py3-setuptools"
checkdepends="
	py3-mock
	py3-openssl
	py3-parameterized
	py3-pytest
	"
source="https://files.pythonhosted.org/packages/source/g/google-api-python-client/google-api-python-client-$pkgver.tar.gz"

builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-google-api-python-client" # Backwards compatibility
provides="py-google-api-python-client=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 -m pytest tests
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# Fix permissions
	_site_packages=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")
	chmod -R a+r "$pkgdir$_site_packages"
}

sha512sums="
86ba1a69f81bd59ab2b6fe967b8d4286af1ca50b0d4939a0cca1b118f6d69938b9a55bd268ce9c11955b547b5a61f3608f50aa000a2ce98563758138a2ce7dd5  google-api-python-client-2.74.0.tar.gz
"
