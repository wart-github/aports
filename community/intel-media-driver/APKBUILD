# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=intel-media-driver
pkgver=23.1.0
pkgrel=0
pkgdesc="Intel Media Driver for VAAPI - Broadwell+ iGPUs"
options="!check" # tests can't run in check(), only on install
url="https://github.com/intel/media-driver"
arch="x86_64"
license="BSD-3-Clause AND MIT"
makedepends="
	cmake
	intel-gmmlib-dev
	libpciaccess-dev
	libva-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/intel/media-driver/archive/intel-media-$pkgver.tar.gz"
builddir="$srcdir/media-driver-intel-media-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DINSTALL_DRIVER_SYSCONF=OFF \
		-DMEDIA_BUILD_FATAL_WARNINGS=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c9764cc6b1a0859ad89e1deb7a26ebe54ee287a6367612e34b7bc4b0100e4ad138d537bc5c987aa0cbd7257850112a36c2be185c9fccad099bc82a956b533775  intel-media-driver-23.1.0.tar.gz
"
