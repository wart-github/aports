# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=kumactl
pkgver=2.0.2
pkgrel=0
pkgdesc="CLI for the multi-zone service mesh Kuma"
url="https://kuma.io"
arch="all !x86" # tests fail
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/kumahq/kuma/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/kuma-$pkgver"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="
	-X github.com/kumahq/kuma/pkg/version.version=$pkgver
	-X github.com/kumahq/kuma/pkg/version.gitTag=v$pkgver
	-X github.com/kumahq/kuma/pkg/version.gitCommit=AlpineLinux
	-X github.com/kumahq/kuma/pkg/version.buildDate=$(date -u "+%Y-%m-%dT%H:%M:%SZ" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	"
	go build -v -o $pkgname -ldflags "$_goldflags" ./app/kumactl

	for shell in bash fish zsh; do
		./$pkgname completion $shell > $pkgname.$shell
	done
}

check() {
	# Only run tests against kumactl-related modules
	go test $(go list ./app/kumactl/... ./pkg/config/app/kumactl/...)
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
a12019045dfa5c19d0a71ad743bab25d5cb95281d42bf978daa5caa12f9f6d1189b96d026c6e375b1d8f2e64afc841b0d991a5f87782e617339f66a2a2eb575b  kumactl-2.0.2.tar.gz
"
