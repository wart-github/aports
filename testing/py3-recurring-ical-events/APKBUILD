# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-recurring-ical-events
pkgver=2.0.0
pkgrel=0
pkgdesc="Python library for recurrence of ical events based on icalendar"
url="https://github.com/niccokunzmann/python-recurring-ical-events"
arch="noarch"
license="LGPL-3.0-only"
depends="python3 py3-icalendar py3-tz py3-tzdata py3-x-wr-timezone"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://github.com/niccokunzmann/python-recurring-ical-events/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/python-recurring-ical-events-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# Disable unnecessary test that adds another dependency.
	pytest -v \
		--ignore test/test_readme.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
676bc074a4d9ddb2bf0aefe8e118eab6e80479bb0d585b9f4c50f4bd49c7adf03e484b34bdf01596bb012c903a8017644ff74026d333f13f5c7df2c4326ab18c  py3-recurring-ical-events-2.0.0.tar.gz
"
