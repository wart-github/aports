# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libx11
pkgver=1.8.3
pkgrel=1
pkgdesc="X11 client-side library"
url="http://xorg.freedesktop.org/"
arch="all"
license="custom:XFREE86"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
depends_dev="libxcb-dev xtrans"
makedepends="$depends_dev xorgproto util-macros xmlto"
source="https://www.x.org/releases/individual/lib/libX11-$pkgver.tar.xz
	allow-x11-renter.patch
	rev-xputback.patch
	"
builddir="$srcdir"/libX11-$pkgver

# secfixes:
#   1.7.1-r0:
#     - CVE-2021-31535
#   1.6.12-r0:
#     - CVE-2020-14363
#   1.6.10-r0:
#     - CVE-2020-14344
#   1.6.6-r0:
#     - CVE-2018-14598
#     - CVE-2018-14599
#     - CVE-2018-14600

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-xcb
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
}

sha512sums="
bc862338fed855986659e9ffa641db6b36c3ac9abced590d1b164e3cc24446671936e3688cdca18393129c4ea41777977eeb37e87d8edc14d6cc5d194a9c0325  libX11-1.8.3.tar.xz
e28fa4c96065e1e3377aaed5eca14c4eafc79b28ba5150e4a7d0b0e88b4da96bd26503b2e5a5100b06ac8de49b219036ff531faa55b9323cc549cf51e1f054ca  allow-x11-renter.patch
fdda7a79f30bbc79b90b364c2c7cf7beb6eb573cb2b13d6913060b6dc352a06012cf5ce939c118ddde41754371bfa61afd5aed1e69284ec9c2e13f6ebddefab0  rev-xputback.patch
"
